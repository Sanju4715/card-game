import React, { Component } from "react";
import { Switch } from "react-router-dom";
import { connect } from "react-redux";

import { UserPublicRoute, UserRoute } from "./UserRoutes";
import UserSignUpLogin from "../Components/UserSignUpLogin/UserSignUpLogin";
import Game from "../Components/Game/Game";

class Routes extends Component {
  render() {
    return (
      <Switch>
        <UserPublicRoute
          path="/"
          restricted={true}
          isUser={this.props.isUser}
          exact
          component={UserSignUpLogin}
        />
        <UserRoute
          path="/game"
          isUser={this.props.isUser}
          exact
          component={Game}
        />
      </Switch>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isUser: state.UserReducer.isUser,
    userId: state.UserReducer.userId,
  };
};

export default connect(mapStateToProps, null)(Routes);
