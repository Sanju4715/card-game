import { Redirect, Route } from "react-router-dom";

export const UserRoute = ({ isUser, component: Comp, ...rest }) => {
  return (
    <Route
      {...rest}
      component={(props) =>
        isUser ? <Comp {...props} /> : <Redirect to="/" {...props} />
      }
    />
  );
};

export const UserPublicRoute = ({
  isUser,
  restricted,
  component: Comp,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      component={(props) =>
        restricted ? (
          isUser ? (
            <Redirect to="/game" {...props} />
          ) : (
            <Comp {...props} />
          )
        ) : (
          <Comp {...props} />
        )
      }
    />
  );
};
