import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";
import Divider from "@material-ui/core/Divider";
import moment from "moment";

import PowerSettingsNewIcon from "@material-ui/icons/PowerSettingsNew";

import { useStyles } from "./UserProfile.css";
import { connect } from "react-redux";
import { userLogout } from "../../../Redux/Actions/Actions";
import TopScore from "../TopScore/TopScore";

class UserProfile extends Component {
  compare = (a, b) => {
    const nameA = a.gameDate;
    const nameB = b.gameDate;

    let comparison = 0;
    if (nameA < nameB) {
      comparison = 1;
    } else if (nameA > nameB) {
      comparison = -1;
    }
    return comparison;
  };

  render() {
    const {
      classes,
      scores,
      userDetails,
      easyScores,
      mediumScores,
      hardScores,
      showScore,
    } = this.props;
    scores.sort(this.compare);
    return (
      <div>
        <div style={{ display: "flex", alignItems: "center" }}>
          <div
            style={{
              textAlign: "left",
              fontSize: 24,
              fontWeight: "bold",
              flexGrow: 1,
            }}
          >
            Dashboard
          </div>
          <Tooltip
            arrow
            title="Logout"
            className={classes.tooltip}
            onClick={this.props.userLogout}
          >
            <PowerSettingsNewIcon
              style={{ color: "#ff0000", width: 30, height: 30 }}
            />
          </Tooltip>
        </div>
        <div>Welcome, {userDetails && userDetails.fullName}</div>
        <Divider />

        <b>Times Game Played</b>
        <ListItem>
          <b style={{ flexGrow: 1 }}>Easy</b>
          <div>{userDetails.easyPlayed}</div>
        </ListItem>
        <ListItem>
          <b style={{ flexGrow: 1 }}>Medium</b>
          <div>{userDetails.mediumPlayed}</div>
        </ListItem>
        <ListItem>
          <b style={{ flexGrow: 1 }}>Hard</b>
          <div>{userDetails.hardPlayed}</div>
        </ListItem>
        <Divider />

        <b>Personal Highest Score</b>
        <ListItem>
          <b style={{ flexGrow: 1 }}>Easy</b>
          <div>{userDetails.easyHighestScore}</div>
        </ListItem>
        <ListItem>
          <b style={{ flexGrow: 1 }}>Meduim</b>
          <div>{userDetails.mediumHighestScore}</div>
        </ListItem>
        <ListItem>
          <b style={{ flexGrow: 1 }}>Hard</b>
          <div>{userDetails.hardHighestScore}</div>
        </ListItem>
        <Divider />

        {showScore === false && (
          <div>
            {scores.length !== 0 && (
              <div>
                <b>My Past Score</b>
                {scores.slice(0, 5).map((score, index) => (
                  <List key={index}>
                    <ListItem>
                      <b style={{ flexGrow: 1 }}>Level </b>
                      <div>{score.level}</div>
                    </ListItem>
                    <ListItem>
                      <b style={{ flexGrow: 1 }}>Total Score</b>
                      <div>{score.totalScore}</div>
                    </ListItem>
                    <ListItem>
                      <b style={{ flexGrow: 1 }}>Time Taken</b>
                      <div>{score.timeTaken}</div>
                    </ListItem>
                    <ListItem divider>
                      <b style={{ flexGrow: 1 }}>Game Date</b>
                      <div>{moment(score.gameDate).format("LL")}</div>
                    </ListItem>
                  </List>
                ))}
              </div>
            )}
          </div>
        )}
        {showScore && (
          <TopScore
            orientation="vertical"
            easyScores={easyScores}
            mediumScores={mediumScores}
            hardScores={hardScores}
          />
        )}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userLogout: () => {
      dispatch(userLogout());
    },
  };
};

export default connect(
  null,
  mapDispatchToProps
)(withStyles(useStyles)(UserProfile));
