export const useStyles = (theme) => ({
  root: { padding: 10 },
  gridRoot: { padding: 20 },
  gridContent: {
    padding: 20,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonRoot: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    [theme.breakpoints.down(700)]: { flexDirection: "column" },
  },
  buttonStyle: {
    backgroundColor: "#004f70 !important",
    borderRadius: 15,
    color: "#fff",
    fontSize: 16,
    fontWeight: "bold",
    textTransform: "none",
    marginRight: 10,
    "&:hover": {
      backgroundColor: "#eee !important",
      borderColor: "#004f70 !important",
      color: "#000",
    },
    [theme.breakpoints.down(700)]: { marginBottom: 10 },
  },
});
