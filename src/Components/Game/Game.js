import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";

import { useStyles } from "./Game.css";
import axios from "axios";
import { url } from "../../Config/Url";
import { connect } from "react-redux";
import UserProfile from "./UserProfile/UserProfile";
import CardGame from "../CardGame/CardGame";
import TopScore from "./TopScore/TopScore";

class Game extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      scores: [],
      easyScores: [],
      mediumScores: [],
      hardScores: [],
      level: "",
      gameStatus: false,
      showScore: false,
      totalPlayed: 0,
    };
  }

  componentDidMount() {
    this.getUserDetails();
    this.getUsers();
  }

  getUserDetails = async () => {
    try {
      const response = await axios.get(`${url}/users`, {
        params: { id: this.props.userId },
      });
      if (response.data.length !== 0) {
        let totalPlayed =
          response.data[0].easyPlayed +
          response.data[0].mediumPlayed +
          response.data[0].hardPlayed;
        this.setState({
          data: response.data[0],
          scores: response.data[0].scores,
          totalPlayed,
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  getUsers = async () => {
    try {
      const easyResponse = await axios.get(`${url}/users`, {
        params: {
          _sort: "easyHighestScore",
          _order: "desc",
          _limit: 10,
        },
      });
      const mediumResponse = await axios.get(`${url}/users`, {
        params: {
          _sort: "mediumHighestScore",
          _order: "desc",
          _limit: 10,
        },
      });
      const hardResponse = await axios.get(`${url}/users`, {
        params: {
          _sort: "hardHighestScore",
          _order: "desc",
          _limit: 10,
        },
      });
      if (easyResponse.data.length !== 0) {
        this.setState({
          easyScores: easyResponse.data,
          mediumScores: mediumResponse.data,
          hardScores: hardResponse.data,
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  handleStart = async (level) => {
    let timesPlayed = 0;
    if (level === "easy") {
      timesPlayed = parseInt(this.state.data.easyPlayed) + 1;
      await axios.patch(`${url}/users/${this.props.userId}`, {
        easyPlayed: timesPlayed,
      });
    } else if (level === "medium") {
      timesPlayed = parseInt(this.state.data.mediumPlayed) + 1;
      await axios.patch(`${url}/users/${this.props.userId}`, {
        mediumPlayed: timesPlayed,
      });
    } else if (level === "hard") {
      timesPlayed = parseInt(this.state.data.hardPlayed) + 1;
      await axios.patch(`${url}/users/${this.props.userId}`, {
        hardPlayed: timesPlayed,
      });
    }
    this.setState({
      level,
      showScore: true,
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container spacing={2} className={classes.gridRoot}>
          <Grid item xs={12} sm={12} md={4} xl={4} lg={4}>
            <Paper style={{ padding: 20 }}>
              <UserProfile
                scores={this.state.scores}
                userDetails={this.state.data}
                showScore={this.state.showScore}
                easyScores={this.state.easyScores}
                mediumScores={this.state.mediumScores}
                hardScores={this.state.hardScores}
              />
            </Paper>
          </Grid>
          <Grid item xs={12} sm={12} md={8} xl={8} lg={8}>
            <Paper style={{ padding: 20 }}>
              {this.state.showScore ? (
                <CardGame
                  level={this.state.level}
                  username={this.state.data.username}
                  scores={this.state.data.scores}
                  easyHighestScore={this.state.data.easyHighestScore}
                  mediumHighestScore={this.state.data.mediumHighestScore}
                  hardHighestScore={this.state.data.hardHighestScore}
                />
              ) : (
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                  }}
                >
                  <p style={{ textAlign: "center" }}>
                    You have played this game total of{" "}
                    <b>{this.state.totalPlayed}</b> times. Will you be able to
                    beat the score in your{" "}
                    <b>
                      {this.state.totalPlayed && this.state.totalPlayed + 1}
                    </b>{" "}
                    game? Are you able to beat this scorers?
                  </p>
                  <div className={classes.buttonRoot}>
                    <Button
                      variant="outlined"
                      disableRipple
                      className={classes.buttonStyle}
                      onClick={() => this.handleStart("easy")}
                    >
                      Easy Level
                    </Button>
                    <Button
                      variant="outlined"
                      disableRipple
                      className={classes.buttonStyle}
                      onClick={() => this.handleStart("medium")}
                    >
                      Medium Level
                    </Button>
                    <Button
                      variant="outlined"
                      disableRipple
                      className={classes.buttonStyle}
                      onClick={() => this.handleStart("hard")}
                    >
                      Hard Level
                    </Button>
                  </div>
                  {this.state.showScore === false && (
                    <TopScore
                      orientation="horizontal"
                      easyScores={this.state.easyScores}
                      mediumScores={this.state.mediumScores}
                      hardScores={this.state.hardScores}
                    />
                  )}
                </div>
              )}
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userId: state.UserReducer.userId,
    isUser: state.UserReducer.isUser,
  };
};

export default connect(mapStateToProps, null)(withStyles(useStyles)(Game));
