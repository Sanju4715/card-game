import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";

class TopScore extends Component {
  render() {
    const { orientation, easyScores, mediumScores, hardScores } = this.props;
    return (
      <div style={{ width: "100%" }}>
        <div style={{ fontSize: 20, fontWeight: "bold" }}>Top 10 Scorers</div>
        <br />
        <Grid container spacing={2}>
          <Grid
            item
            xs={12}
            sm={12}
            md={orientation === "horizontal" ? 4 : 12}
            xl={orientation === "horizontal" ? 4 : 12}
            lg={orientation === "horizontal" ? 4 : 12}
          >
            <div style={{ fontSize: 16, fontWeight: "bold" }}>Easy</div>
            <Divider />
            {easyScores.length !== 0 &&
              easyScores.map((score) => (
                <ListItem key={score.id}>
                  <b style={{ flexGrow: 1 }}>{score.fullName}</b>
                  <b>{score.easyHighestScore}</b>
                </ListItem>
              ))}
          </Grid>
          <Grid
            item
            xs={12}
            sm={12}
            md={orientation === "horizontal" ? 4 : 12}
            xl={orientation === "horizontal" ? 4 : 12}
            lg={orientation === "horizontal" ? 4 : 12}
          >
            <div style={{ fontSize: 16, fontWeight: "bold" }}>Medium</div>
            <Divider />
            {mediumScores.length !== 0 &&
              mediumScores.map((score) => (
                <ListItem key={score.id}>
                  <b style={{ flexGrow: 1 }}>{score.fullName}</b>
                  <b>{score.mediumHighestScore}</b>
                </ListItem>
              ))}
          </Grid>
          <Grid
            item
            xs={12}
            sm={12}
            md={orientation === "horizontal" ? 4 : 12}
            xl={orientation === "horizontal" ? 4 : 12}
            lg={orientation === "horizontal" ? 4 : 12}
          >
            <div style={{ fontSize: 16, fontWeight: "bold" }}>Hard</div>
            <Divider />
            {hardScores.length !== 0 &&
              hardScores.map((score) => (
                <ListItem key={score.id}>
                  <b style={{ flexGrow: 1 }}>{score.fullName}</b>
                  <b>{score.hardHighestScore}</b>
                </ListItem>
              ))}
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default TopScore;
