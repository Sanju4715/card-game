import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import ListItem from "@material-ui/core/ListItem";

import AcUnitRoundedIcon from "@material-ui/icons/AcUnitRounded";
import AddAlertRoundedIcon from "@material-ui/icons/AddAlertRounded";
import AdjustRoundedIcon from "@material-ui/icons/AdjustRounded";
import ArrowDropDownCircleRoundedIcon from "@material-ui/icons/ArrowDropDownCircleRounded";
import AssignmentRoundedIcon from "@material-ui/icons/AssignmentRounded";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import BeachAccessIcon from "@material-ui/icons/BeachAccess";
import BookmarkIcon from "@material-ui/icons/Bookmark";
import CameraIcon from "@material-ui/icons/Camera";

import GameRules from "./GameRules/GameRules";
import { useStyles } from "./CardGame.css";
import MenuItems from "./MenuItems/MenuItems";
import { withRouter } from "react-router-dom";
import axios from "axios";
import { url } from "../../Config/Url";
import { connect } from "react-redux";

class CardGame extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gameStatus: false,
      timer: 0,
      time: {},
      timeTaken: {},
      seconds:
        this.props.level === "easy"
          ? 60
          : this.props.level === "medium"
          ? 120
          : this.props.level === "hard" && 180,
      gameTime: true,
      score: 0,
      remainingSeconds: 0,
      bonusScore: 0,
      items: [],
      easyItems: [
        {
          image: <AcUnitRoundedIcon style={{ width: 50, height: 50 }} />,
          name: "Image1",
          status: false,
        },
        {
          image: <AddAlertRoundedIcon style={{ width: 50, height: 50 }} />,
          name: "Image2",
          status: false,
        },
        {
          image: <AdjustRoundedIcon style={{ width: 50, height: 50 }} />,
          name: "Image3",
          status: false,
        },
      ],
      mediumItems: [
        {
          image: <AcUnitRoundedIcon style={{ width: 50, height: 50 }} />,
          name: "Image1",
          status: false,
        },
        {
          image: <AddAlertRoundedIcon style={{ width: 50, height: 50 }} />,
          name: "Image2",
          status: false,
        },
        {
          image: <AdjustRoundedIcon style={{ width: 50, height: 50 }} />,
          name: "Image3",
          status: false,
        },
        {
          image: (
            <ArrowDropDownCircleRoundedIcon style={{ width: 50, height: 50 }} />
          ),
          name: "Image4",
          status: false,
        },
        {
          image: <AssignmentRoundedIcon style={{ width: 50, height: 50 }} />,
          name: "Image5",
          status: false,
        },
        {
          image: <AccountBalanceIcon style={{ width: 50, height: 50 }} />,
          name: "Image6",
          status: false,
        },
      ],
      hardItems: [
        {
          image: <AcUnitRoundedIcon style={{ width: 50, height: 50 }} />,
          name: "Image1",
          status: false,
        },
        {
          image: <AddAlertRoundedIcon style={{ width: 50, height: 50 }} />,
          name: "Image2",
          status: false,
        },
        {
          image: <AdjustRoundedIcon style={{ width: 50, height: 50 }} />,
          name: "Image3",
          status: false,
        },
        {
          image: (
            <ArrowDropDownCircleRoundedIcon style={{ width: 50, height: 50 }} />
          ),
          name: "Image4",
          status: false,
        },
        {
          image: <AssignmentRoundedIcon style={{ width: 50, height: 50 }} />,
          name: "Image5",
          status: false,
        },
        {
          image: <AccountBalanceIcon style={{ width: 50, height: 50 }} />,
          name: "Image6",
          status: false,
        },
        {
          image: <BeachAccessIcon style={{ width: 50, height: 50 }} />,
          name: "Image7",
          status: false,
        },
        {
          image: <BookmarkIcon style={{ width: 50, height: 50 }} />,
          name: "Image8",
          status: false,
        },
        {
          image: <CameraIcon style={{ width: 50, height: 50 }} />,
          name: "Image9",
          status: false,
        },
      ],
      waitStatus: false,
      tempIndex: "",
    };
  }

  componentDidMount() {
    let timeLeft = this.secondsToTime(this.state.seconds);
    this.setState({ time: timeLeft });
    this.setItems(
      this.props.level === "easy"
        ? this.state.easyItems
        : this.props.level === "medium"
        ? this.state.mediumItems
        : this.props.level === "hard" && this.state.hardItems
    );
  }

  setItems = (items) => {
    let tempList = [...items, ...items];
    tempList.sort(() => Math.random() - 0.5);
    this.setState({ items: tempList });
  };

  secondsToTime = (secs) => {
    let hours = Math.floor(secs / (60 * 60));

    let divisor_for_minutes = secs % (60 * 60);
    let minutes = Math.floor(divisor_for_minutes / 60);

    let divisor_for_seconds = divisor_for_minutes % 60;
    let seconds = Math.ceil(divisor_for_seconds);

    let obj = {
      h: hours,
      m: minutes < 10 ? "0" + minutes : minutes,
      s: seconds < 10 ? "0" + seconds : seconds,
    };
    return obj;
  };

  startTimer = async () => {
    this.setState({ gameStatus: true });
    if (this.state.timer === 0 && this.state.seconds > 0) {
      this.setState({ timer: setInterval(this.countDown, 1000) });
    }
  };

  countDown = () => {
    // Remove one second, set state so a re-render happens.
    let seconds = this.state.seconds - 1;
    this.setState({
      time: this.secondsToTime(seconds),
      seconds: seconds,
    });

    // Check if we're at zero.
    if (seconds === 0) {
      clearInterval(this.state.timer);
      this.setState({ gameTime: false });
    }
  };

  calculateSeconds = (minute, second) => {
    let tempSecond = minute * 60;
    tempSecond = tempSecond + second;
    return tempSecond;
  };

  handleCheckStatus = () => {
    let checkStatus = this.state.items.filter((data) => data.status === false);
    if (checkStatus.length === 1 || checkStatus.length === 0) {
      return true;
    } else if (checkStatus.length > 1) {
      return false;
    }
  };

  handleFirstClick = (index) => {
    let status = this.handleCheckStatus();
    if (status === false) {
      let tempObj = {
        image: this.state.items[index].image,
        name: this.state.items[index].name,
        status: true,
      };
      this.state.items.splice(index, 1, tempObj);
      let newArr = this.state.items;
      this.setState({
        tempIndex: index,
        items: newArr,
        waitStatus: true,
      });
    }
  };

  handleSecondClick = async (index, imageName) => {
    if (this.state.items[this.state.tempIndex].name === imageName) {
      let tempObj = {
        image: this.state.items[index].image,
        name: this.state.items[index].name,
        status: true,
      };
      this.state.items.splice(index, 1, tempObj);
      let newArr = this.state.items;
      let score = this.state.score + 10;
      if (this.handleCheckStatus()) {
        clearInterval(this.state.timer);
        let remainingSeconds = this.calculateSeconds(
          this.state.time.m,
          this.state.time.s
        );
        let totalSeconds =
          this.props.level === "easy"
            ? 60
            : this.props.level === "medium"
            ? 120
            : this.props.level === "hard" && 180;
        let timeTaken = this.secondsToTime(
          parseInt(totalSeconds) - parseInt(remainingSeconds)
        );
        let bonusScore =
          remainingSeconds > 180 ? remainingSeconds * 4 : remainingSeconds * 2;
        let totalScore = score + bonusScore;
        if (this.props.level === "easy") {
          if (totalScore > this.props.easyHighestScore) {
            await axios.patch(`${url}/users/${this.props.userId}`, {
              easyHighestScore: totalScore,
            });
          }
        } else if (this.props.level === "medium") {
          if (totalScore > this.props.mediumHighestScore) {
            await axios.patch(`${url}/users/${this.props.userId}`, {
              mediumHighestScore: totalScore,
            });
          }
        } else if (this.props.level === "hard") {
          if (totalScore > this.props.hardHighestScore) {
            await axios.patch(`${url}/users/${this.props.userId}`, {
              hardHighestScore: totalScore,
            });
          }
        }
        let tempScores = this.props.scores;
        tempScores.push({
          level: this.props.level,
          totalScore: totalScore,
          timeTaken: timeTaken.m + ":" + timeTaken.s,
          gameDate: new Date(),
        });
        await axios.patch(`${url}/users/${this.props.userId}`, {
          scores: tempScores,
        });
        this.setState({
          gameTime: false,
          score,
          remainingSeconds,
          timeTaken,
          bonusScore,
        });
      } else {
        this.setState({
          items: newArr,
          tempIndex: "",
          score,
          waitStatus: false,
        });
      }
    } else {
      let tempObj = {
        image: this.state.items[this.state.tempIndex].image,
        name: this.state.items[this.state.tempIndex].name,
        status: false,
      };
      this.state.items.splice(this.state.tempIndex, 1, tempObj);
      let newArr = this.state.items;
      this.setState({
        items: newArr,
        waitStatus: false,
      });
    }
  };

  render() {
    const { classes, username } = this.props;
    return (
      <div>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            fontSize: 20,
            fontWeight: "bold",
          }}
        >
          <div style={{ flexGrow: 1 }}>
            {this.state.gameStatus === false
              ? `Are You Ready, ${username}?`
              : `Card Game`}
          </div>
          <div>
            Time Left: {this.state.time.m}:{this.state.time.s}
          </div>
        </div>
        <br />

        <div>Current Level: {this.props.level}</div>
        <br />
        {this.state.gameTime && <div>Score {this.state.score}</div>}
        <br />
        {this.state.gameStatus ? (
          <div>
            {this.state.gameTime ? (
              <div>
                <MenuItems
                  score={this.state.score}
                  items={this.state.items}
                  waitStatus={this.state.waitStatus}
                  handleFirstClick={this.handleFirstClick}
                  handleSecondClick={this.handleSecondClick}
                />
              </div>
            ) : (
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <b style={{ textAlign: "center" }}>
                  Congratulations! You have completed the game.
                </b>
                <ListItem>
                  <b>Current Score: {this.state.score}</b>
                </ListItem>
                <ListItem>
                  <b>
                    Time Taken: {this.state.timeTaken.m}:
                    {this.state.timeTaken.s}
                  </b>
                </ListItem>
                <ListItem>
                  <b>
                    Time Remaining: {this.state.time.m}:{this.state.time.s}
                  </b>
                </ListItem>
                <ListItem divider>
                  <b>Bonus Score: {this.state.bonusScore}</b>
                </ListItem>
                <ListItem>
                  <b>Total Score: {this.state.score + this.state.bonusScore}</b>
                </ListItem>
                <Button
                  variant="outlined"
                  className={classes.buttonStyle}
                  onClick={() => this.props.history.push(`/game`)}
                >
                  Restart Game
                </Button>
              </div>
            )}
          </div>
        ) : (
          <div>
            <Button
              variant="outlined"
              disableRipple
              className={classes.buttonStyle}
              onClick={this.startTimer}
            >
              Start Game
            </Button>
            <GameRules level={this.props.level} />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userId: state.UserReducer.userId,
  };
};

export default connect(
  mapStateToProps,
  null
)(withStyles(useStyles)(withRouter(CardGame)));
