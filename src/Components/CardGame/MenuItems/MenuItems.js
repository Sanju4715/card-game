import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Flip from "react-reveal/Flip";

import { useStyles } from "./MenuItems.css";

class MenuItems extends Component {
  render() {
    const { classes, items, waitStatus, handleFirstClick, handleSecondClick } =
      this.props;
    return (
      <Grid container spacing={3}>
        {items.map((item, index) => (
          <Grid item xs={6} sm={6} md={4} xl={4} lg={4} key={index}>
            {item.status ? (
              <Flip left delay={100}>
                <Paper className={classes.paper}>{item.image}</Paper>
              </Flip>
            ) : (
              <Flip left delay={100}>
                <Paper
                  className={classes.paper}
                  onClick={
                    waitStatus
                      ? () => handleSecondClick(index, item.name)
                      : () => handleFirstClick(index)
                  }
                >
                  Select
                </Paper>
              </Flip>
            )}
          </Grid>
        ))}
      </Grid>
    );
  }
}

export default withStyles(useStyles)(MenuItems);
