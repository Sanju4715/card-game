export const useStyles = (theme) => ({
  paper: {
    padding: 10,
    minHeight: "10vh",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    "&:hover": {
      cursor: "pointer",
      opacity: 0.7,
    },
  },
});
