export const useStyles = (theme) => ({
  buttonStyle: {
    backgroundColor: "#004f70 !important",
    borderRadius: 15,
    color: "#fff",
    fontSize: 16,
    fontWeight: "bold",
    textTransform: "none",
    "&:hover": {
      backgroundColor: "#eee !important",
      borderColor: "#004f70 !important",
      color: "#000",
    },
  },
});
