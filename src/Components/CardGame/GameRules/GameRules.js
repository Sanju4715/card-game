import React, { Component } from "react";

class GameRules extends Component {
  render() {
    return (
      <ol style={{ textAlign: "left" }}>
        <li>The game starts when user click on the start game button.</li>
        <li>The game consist of nxn cards that are to be matched.</li>
        <li>When cards are matched, the user will be granted with points.</li>
        <li>
          Select Any two cards and see if they matched. If the card matched then
          score will be added. If cards are not matched, then try again.
        </li>
        <li>The card which are matched will not be flipped.</li>
        <li>If cards are failed to match then they will be flipped back.</li>
        <li>
          Matched the card within the time limit.
          {this.props.level === "easy"
            ? `The time limit for easy level is 1 minute`
            : this.props.level === "medium"
            ? `The time limit for medium level is 2 minutes`
            : `The time limit for hard level is 3 minutes`}
        </li>
        <li>Bonus score will be granted to the user for the time remaining.</li>
      </ol>
    );
  }
}

export default GameRules;
