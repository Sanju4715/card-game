import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider";
import InputAdornment from "@material-ui/core/InputAdornment";
import Button from "@material-ui/core/Button";
import UUID from "node-uuid";

import NameIcon from "../../../Assets/Logo/Name.svg";
import UsernameIcon from "../../../Assets/Logo/Username.svg";
import { useStyles } from "../UserSignUpLogin.css";
import axios from "axios";
import { url } from "../../../Config/Url";
import { userLogin } from "../../../Redux/Actions/Actions";
import { connect } from "react-redux";

class UserSignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        fullName: "",
        username: "",
      },
      usernameError: "",
    };
  }

  handleChange = (e) => {
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value },
    });
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const checkResponse = await axios.get(`${url}/users`, {
        params: { username: this.state.data.username },
      });
      if (checkResponse.data.length !== 0) {
        this.setState({ usernameError: "Username Already exists" });
      } else {
        let data = {
          id: UUID.v4(),
          fullName: this.state.data.fullName,
          username: this.state.data.username,
          scores: [],
          times: [
            { level: "easy", played: 0 },
            { level: "medium", played: 0 },
            { level: "hard", played: 0 },
          ],
          easyHighestScore: 0,
          mediumHighestScore: 0,
          hardHighestScore: 0,
          gamesPlayed: 0,
        };
        const postResponse = await axios.post(`${url}/users`, data);
        if (postResponse.data.id) {
          this.props.userLogin(postResponse.data.id);
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.contentRoot}>
        <div style={{ textAlign: "left", fontSize: 20, fontWeight: "bold" }}>
          SignUp
        </div>
        <Divider />
        <br />
        <form onSubmit={this.handleSubmit}>
          <Paper className={classes.inputPaper}>
            <TextField
              name="fullName"
              type="text"
              fullWidth
              required
              autoComplete="off"
              margin="normal"
              variant="outlined"
              placeholder="User's FullName"
              style={{ outline: "none" }}
              InputProps={{
                classes: {
                  root: classes.inputRoot,
                  notchedOutline: classes.notchedOutline,
                  focused: classes.focused,
                },
                startAdornment: (
                  <InputAdornment position="start">
                    <img
                      src={NameIcon}
                      alt="Name"
                      style={{
                        width: 25,
                        height: 25,
                      }}
                    />
                  </InputAdornment>
                ),
              }}
              InputLabelProps={{
                classes: {
                  root: classes.labelRoot,
                  focused: classes.labelFocused,
                },
                shrink: true,
              }}
              value={this.state.data.fullName}
              onChange={this.handleChange}
            />
          </Paper>
          <br />
          <Paper className={classes.inputPaper}>
            <TextField
              name="username"
              type="username"
              fullWidth
              required
              autoComplete="off"
              margin="normal"
              variant="outlined"
              placeholder="Enter username"
              style={{ outline: "none" }}
              error={this.state.usernameError ? true : false}
              helperText={this.state.usernameError}
              InputProps={{
                classes: {
                  root: classes.inputRoot,
                  notchedOutline: classes.notchedOutline,
                  focused: classes.focused,
                },
                startAdornment: (
                  <InputAdornment position="start">
                    <img
                      src={UsernameIcon}
                      alt="Username"
                      style={{
                        width: 25,
                        height: 25,
                      }}
                    />
                  </InputAdornment>
                ),
              }}
              InputLabelProps={{
                classes: {
                  root: classes.labelRoot,
                  focused: classes.labelFocused,
                },
                shrink: true,
              }}
              value={this.state.data.username}
              onChange={this.handleChange}
            />
          </Paper>

          <br />
          <Button
            type="submit"
            variant="outlined"
            className={classes.buttonStyle}
          >
            SignUp
          </Button>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userLogin: (userId) => {
      dispatch(userLogin(userId));
    },
  };
};

export default connect(
  null,
  mapDispatchToProps
)(withStyles(useStyles)(UserSignUp));
