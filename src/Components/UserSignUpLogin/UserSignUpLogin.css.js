export const useStyles = (theme) => ({
  root: {
    minHeight: "90vh",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  paperRoot: {
    display: "flex",
    padding: 10,
    boxShadow: "0 4px 10px 0 rgba(0,0,0,.25)",
    backgroundColor: "#eee",
  },
  contentRoot: { padding: 10 },
  inputPaper: {
    borderRadius: 10,
    boxShadow: "0 4px 10px 0 rgba(0,0,0,.25)",
  },
  inputRoot: {
    height: 30,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 5,
  },
  notchedOutline: {
    borderColor: "#fff !important",
  },
  labelFocused: {
    color: "#fff !important",
  },
  focused: {
    "& $notchedOutline": {
      borderColor: "#fff !important",
    },
  },
  buttonStyle: {
    backgroundColor: "#004f70 !important",
    borderRadius: 15,
    color: "#fff",
    fontSize: 16,
    fontWeight: "bold",
    textTransform: "none",
    "&:hover": {
      backgroundColor: "#eee !important",
      borderColor: "#004f70 !important",
      color: "#000",
    },
  },
});
