import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider";

import { useStyles } from "./UserSignUpLogin.css";
import UserLogin from "./UserLogin/UserLogin";
import UserSignUp from "./UserSignUp/UserSignUp";

class UserSignUpLogin extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <div style={{ fontSize: 24, fontWeight: "bold" }}>
          Welcome to the game
        </div>
        <br />
        <Paper className={classes.paperRoot}>
          <UserLogin />
          <Divider flexItem style={{ height: "auto", width: 1 }} />
          <UserSignUp />
        </Paper>
      </div>
    );
  }
}

export default withStyles(useStyles)(UserSignUpLogin);
