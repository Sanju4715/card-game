import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider";
import InputAdornment from "@material-ui/core/InputAdornment";
import Button from "@material-ui/core/Button";

import UsernameIcon from "../../../Assets/Logo/Username.svg";
import { useStyles } from "../UserSignUpLogin.css";
import axios from "axios";
import { url } from "../../../Config/Url";
import { userLogin } from "../../../Redux/Actions/Actions";

class UserLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      usernameError: "",
    };
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.get(`${url}/users`, {
        params: { username: this.state.username },
      });
      if (response.data.length === 0) {
        this.setState({ usernameError: "Username Not Found" });
      } else {
        this.props.userLogin(response.data[0].id);
      }
    } catch (error) {
      console.log(error.response);
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.contentRoot}>
        <div style={{ textAlign: "left", fontSize: 20, fontWeight: "bold" }}>
          Login
        </div>
        <Divider />
        <br />
        <form onSubmit={this.handleSubmit}>
          <Paper className={classes.inputPaper}>
            <TextField
              name="username"
              type="username"
              fullWidth
              required
              autoComplete="off"
              margin="normal"
              variant="outlined"
              placeholder="Enter username"
              style={{ outline: "none" }}
              error={this.state.usernameError ? true : false}
              helperText={this.state.usernameError}
              InputProps={{
                classes: {
                  root: classes.inputRoot,
                  notchedOutline: classes.notchedOutline,
                  focused: classes.focused,
                },
                startAdornment: (
                  <InputAdornment position="start">
                    <img
                      src={UsernameIcon}
                      alt="Username"
                      style={{
                        width: 25,
                        height: 25,
                      }}
                    />
                  </InputAdornment>
                ),
              }}
              InputLabelProps={{
                classes: {
                  root: classes.labelRoot,
                  focused: classes.labelFocused,
                },
                shrink: true,
              }}
              value={this.state.username}
              onChange={this.handleChange}
            />
          </Paper>
          <br />
          <Button
            type="submit"
            variant="outlined"
            className={classes.buttonStyle}
          >
            Login
          </Button>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userLogin: (userId) => {
      dispatch(userLogin(userId));
    },
  };
};

export default connect(
  null,
  mapDispatchToProps
)(withStyles(useStyles)(UserLogin));
