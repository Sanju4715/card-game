import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { persistStore, persistReducer } from "redux-persist";
import { composeWithDevTools } from "redux-devtools-extension";
import { CookieStorage } from "redux-persist-cookie-storage";
import Cookies from "cookies-js";

import Reducer from "../Reducers/ReducerIndex";

const persistConfig = {
  key: "root",
  storage: new CookieStorage(Cookies, {}),
};

const persistorReducer = persistReducer(persistConfig, Reducer);
export const store = createStore(
  persistorReducer,
  composeWithDevTools(applyMiddleware(thunk))
);
export const persistor = persistStore(store);

export default (store, persistor);
