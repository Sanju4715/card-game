import { USER_LOGIN, USER_LOGOUT } from "../Types/Types";

const initState = {
  userId: "",
  isUser: false,
};

const UserReducer = (state = initState, action) => {
  switch (action.type) {
    case USER_LOGIN:
      return { ...state, userId: action.payload, isUser: true };

    case USER_LOGOUT:
      return { ...state, userId: "", isUser: false };

    default:
      return state;
  }
};

export default UserReducer;
