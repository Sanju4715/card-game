export const userLogin = (userId) => (dispatch) => {
  dispatch({ type: "USER_LOGIN", payload: userId });
};

export const userLogout = () => (dispatch) => {
  dispatch({ type: "USER_LOGOUT" });
};
